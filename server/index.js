
const admin = require("firebase-admin");
const Particle = require('particle-api-js');
const moment = require('moment');
const settings = require('./settings.json');
const serviceAccount = require("./inclusion-51fec-firebase-adminsdk-4x6m2-5e8a7de13a.json");

let token;
let particle = null;

const login = async (props) => {
  particle = new Particle();
  try{
    let data = await particle.login({ username: props.user, password: props.password });
    token = data.body.access_token;
    listener();
  }
}

const listener = async () => {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://inclusion-51fec.firebaseio.com"
  });
  let db = admin.firestore();
  db.settings({ timestampsInSnapshots: true });
  let doc = db.collection('time').doc('timetable');
  let observer = doc.onSnapshot(docSnapshot => {
    console.log(docSnapshot.data());
    let currentTime = moment();
    let startTime = moment(docSnapshot.data().start, "HH:mm:ss");
    let endTime = moment(docSnapshot.data().end, "HH:mm:ss");
    inRange = currentTime.isBetween(startTime , endTime);
    if(inRange){
      console.log("on");
      //process({ name: "process", argument: "off" });
    }
    else{
      console.log("off");
      //process({ name: "process", argument: "on" });
    }
  }, err => {
    console.log(err);
  });
}

const process = async (options) => {
  try{
    let processFn = await particle.callFunction({
      deviceId: settings.deviceId,
      name: options.name,
      argument: options.argument,
      auth: token
    });
    console.log('Function called succesfully:', data);
  }
}

login({ user: settings.user, password: settings.password });
