// This #include statement was automatically added by the Particle IDE.
#include <Particle.h>

int currentLed = D0;
int status = 0;


void setup() {
    Particle.function("process", process);
    Particle.function("toggle", toggle);
    pinMode(D0, OUTPUT);
    pinMode(D1, OUTPUT);
    pinMode(D2, OUTPUT);
    pinMode(D3, OUTPUT);
    pinMode(D4, OUTPUT);
    pinMode(D5, OUTPUT);
    pinMode(D6, OUTPUT);
    pinMode(D7, OUTPUT);
    digitalWrite(D0, LOW);
    digitalWrite(D1, LOW);
    digitalWrite(D2, LOW);
    digitalWrite(D3, LOW);
    digitalWrite(D4, LOW);
    digitalWrite(D5, LOW);
    digitalWrite(D6, LOW);
    digitalWrite(D7, LOW);
}

void loop() {
    if(status == 0){
        digitalWrite(currentLed, LOW);
    }
    else{
        digitalWrite(currentLed, HIGH);
    }

}

int process(String command){
    if(command=="off"){
        status = 1;
        digitalWrite(D0, HIGH);
        digitalWrite(D1, HIGH);
        digitalWrite(D2, HIGH);
        digitalWrite(D3, HIGH);
        digitalWrite(D4, HIGH);
        digitalWrite(D5, HIGH);
        digitalWrite(D6, HIGH);
        digitalWrite(D7, HIGH);
        return 1;
    }
    else if(command=="on"){
        status = 0;
        digitalWrite(D0, LOW);
        digitalWrite(D1, LOW);
        digitalWrite(D2, LOW);
        digitalWrite(D3, LOW);
        digitalWrite(D4, LOW);
        digitalWrite(D5, LOW);
        digitalWrite(D6, LOW);
        digitalWrite(D7, LOW);
        return 0;
    }
    else{
        return -1;
    }
}

int toggle(String command){

    if(command == "D0:HIGH"){ currentLed = D0; status = 1; }
    if(command == "D1:HIGH"){ currentLed = D1; status = 1; }
    if(command == "D2:HIGH"){ currentLed = D2; status = 1; }
    if(command == "D3:HIGH"){ currentLed = D3; status = 1; }
    if(command == "D4:HIGH"){ currentLed = D4; status = 1; }
    if(command == "D5:HIGH"){ currentLed = D5; status = 1; }
    if(command == "D6:HIGH"){ currentLed = D6; status = 1; }
    if(command == "D7:HIGH"){ currentLed = D7; status = 1; }
    if(command == "D0:LOW"){ currentLed = D0; status = 0; }
    if(command == "D1:LOW"){ currentLed = D1; status = 0; }
    if(command == "D2:LOW"){ currentLed = D2; status = 0; }
    if(command == "D3:LOW"){ currentLed = D3; status = 0; }
    if(command == "D4:LOW"){ currentLed = D4; status = 0; }
    if(command == "D5:LOW"){ currentLed = D5; status = 0; }
    if(command == "D6:LOW"){ currentLed = D6; status = 0; }
    if(command == "D7:LOW"){ currentLed = D7; status = 0; }


    return status;
}
