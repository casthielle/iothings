import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import RootStack from './src/app.js';
import { store, persistor } from './src/store';


type Props = {};
class App extends Component<Props> {
  render() {
    return (
      <Provider store={ store }>
        <PersistGate loading={null} persistor={ persistor }>
          <RootStack/>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
