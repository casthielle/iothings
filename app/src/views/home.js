import React, { Component } from "react";
import { Animated } from 'react-native';
import { connect } from 'react-redux';

import { Tile } from "./components/globals";
import { HomeLayout } from './containers/layouts';

setProps = (state) => { return { user: state.user }; };

class Home extends Component {

  constructor(props){
      super(props);
      this.state = {
        opacity: new Animated.Value(0),
      };
  }

  componentDidMount(){
    Animated.timing(
      this.state.opacity,
      {
        toValue: 1,
        duration: 500
      }
    ).start();
  }

  render() {
    return (
      <Animated.View style={{ flex: 1, opacity: this.state.opacity }}>
        <HomeLayout user={ this.props.user } back={() => this.props.navigation.goBack()} action={ () => this.props.navigation.navigate('profile') }>
          <Tile onPress={() => this.props.navigation.navigate("illumination")} text="Iluminación" />
        </HomeLayout>
      </Animated.View>
    );
  }
}

export default connect(setProps)(Home);
