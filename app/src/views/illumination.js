import React, { Component } from 'react';
import { ScrollView, FlatList, Animated } from 'react-native';
import { connect } from 'react-redux';
import { Button, EmptyItem, Item, Separator, ToolBar, Card, Dialog, InputText } from "./components/globals";
import Particle from '../apis/particle';
import Firebase from '../apis/firebase';
import { IlluminationLayout } from './containers/layouts';

setProps = (state) => { return { user: state.user }; };

class Illumination extends Component {

  constructor(props) {
    super(props);
    this.state = {
      firebase: Firebase.app(),
      firestore: Firebase.firestore(),
      opacity: new Animated.Value(0),
      modalAreaVisible: false,
      modalTimeVisible: false,
      start: "08:00:00",
      end: "19:00:00",
      toggle: true,
      newDaviceName: "",
      devices: []
    };

    this.state.firestore.collection("areas").onSnapshot(querySnapshot => {
      let devices = [];
      querySnapshot.forEach((doc) => {
        let device = doc.data();
        let state = (device.state) ? "HIGH" : "LOW";
        let settings = {
          fn: "toggle",
          body: {
            arg: device.id + ":" + state
          }
        }
        device.uid = doc.id;
        devices.push(device);
        Particle.process(settings);
      });
      this.setState({ devices: devices });
    });
    this.state.firestore.collection("time").doc("timetable").onSnapshot(doc => {
      this.setState({ end: doc.data().end });
      this.setState({ start: doc.data().start });
    });
  }

  toggle = async (item) => {
    this.state.firestore.collection("areas").doc(item.uid).update({
      state: !item.state
    });
  }
  toggleAll = async () => {
    this.state.devices.map((device) => {
      this.state.firestore.collection("areas").doc(device.uid).update({
        state: this.state.toggle
      });
    });
    this.setState({ toggle: !this.state.toggle });
  }
  saveTime = async () => {
    this.state.firestore.collection("time").doc("timetable").update({
      start: this.state.start,
      end: this.state.end
    });
    this.toggleModal('setTime',false)
  }
  saveArea = async () => {
    if(this.state.newDaviceName != ""){
      this.state.firestore.collection("areas").add({
        id: this._getId(),
        text: this.state.newDaviceName,
        state: false
      });
      this.setState({ newDaviceName: "" });
      this.toggleModal('addArea', false);
    }
  }
  removeArea = async (item) => {
    this.state.firestore.collection("areas").doc(item.uid).delete();
  }
  _getId = (id) => {
    id = id || 0;
    let match = this.state.devices.find(device => device.id == "D"+id);
    if(!match){
      return "D"+id;
    }
    return this._getId(id+1);
  }
  toggleModal = (modal, visible) => {
    if(modal == 'addArea'){
      this.setState({ modalAreaVisible: visible });
    }
    if(modal == 'setTime'){
      this.setState({ modalTimeVisible: visible });
    }
  }
  closeModal = () => {
    this.setState({ modalAreaVisible: false });
    this.setState({ modalTimeVisible: false });
  }
  handlerChangeDeviceName = (deviceName) => {
    return this.setState({ newDaviceName: deviceName });
  }
  handlerChangeStartTime = (startTime) => {
    return this.setState({ start: startTime });
  }
  handlerChangeEndTime = (endTime) => {
    return this.setState({ end: endTime });
  }

  componentDidMount(){
    Animated.timing(
      this.state.opacity,
      {
        toValue: 1,
        duration: 500
      }
    ).start();
  }
  render(){
    return (
      <Animated.View style={{ flex:1, opacity: this.state.opacity }}>
        <IlluminationLayout user={ this.props.user } back={() => this.props.navigation.goBack()} action={() => this.props.navigation.navigate('profile')}>

          <Dialog visible={ this.state.modalAreaVisible } title="Agregar Area" onClose={ this.closeModal } onAction={ this.saveArea } icon="save">
            <InputText onChange={ this.handlerChangeDeviceName } placeholder="Nombre" backgroundColor="#f4f4f4"/>
          </Dialog>

          <Dialog visible={ this.state.modalTimeVisible } title="Configurar Horario" onClose={ this.closeModal } onAction={ this.saveTime } icon="save">
            <InputText onChange={ this.handlerChangeStartTime } value={ this.state.start } placeholder="Hora de Encendido" backgroundColor="#f4f4f4"/>
            <InputText onChange={ this.handlerChangeEndTime } value={ this.state.end } placeholder="Hora de Apagado" backgroundColor="#f4f4f4"/>
          </Dialog>

          <Card>
            <ToolBar title="Horario:" subtitle={ this.state.start + " - " + this.state.end } icon="access-time">
              <Button name="edit" color="#000" onPress={() => this.toggleModal('setTime', true) }/>
            </ToolBar>
          </Card>

          <Card scroll>
            <ToolBar title="Areas" icon="developer-board" onPress={ () => { this.toggleAll(); } }>
              <Button name="add-circle-outline" color="#000" onPress={() => this.toggleModal('addArea', true) }/>
            </ToolBar>
            <ScrollView>
              <FlatList
                keyExtractor={ item => item.uid.toString() }
                data={ this.state.devices }
                renderItem={ ({item}) => <Item {...item} onPress={ () => { this.toggle(item); } }><Button onPress={ () => this.removeArea(item) } color="#000" name="remove-circle-outline"/></Item> }
                ListEmptyComponent={ () => <EmptyItem text="No hay dispositivos conectados." /> }
                />
            </ScrollView>
          </Card>

        </IlluminationLayout>
      </Animated.View>
    );
  }
}


export default connect(setProps)(Illumination);
