import React, { Component } from 'react';
import { ScrollView, FlatList, Animated } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Firebase from '../apis/firebase';
import { ProfileLayout } from './containers/layouts';
import { EmptyItem, SimpleItem, Separator, ToolBar, Card } from "./components/globals";

setProps = (state) => { return { user: state.user }; };

class Profile extends Component {

  constructor(props){
    super(props);
    this.state = {
      opacity: new Animated.Value(1),
      infoList: [{
        key: "1",
        property: "Email",
        value: props.user.email,
        type: "item-list"
      },{
        key: "2",
        property: "Telefono",
        value: props.user.phonenumber,
        type: "item-list"
      },{
        key: "3",
        property: "Dirección",
        value: props.user.address,
        type: "item-list"
      },{
        key: "4",
        property: "Rol",
        value: props.user.role,
        type: "item-list"
      },{
        key: "5",
        property: "exit",
        value: "Cerrar Sesión",
        type: "item-touchable"
      }]
    };
  }

  _exit = () => {
    Firebase.auth().signOut().then(() => {
      this.props.dispatch({
        type: "SET_USER_DATA",
        payload: {
          user: {}
        }
      });
      this.props.navigation.navigate('Auth');
    }).catch(error => {
      console.log("Error trying to log out.", error);
    });
  }

  componentDidMount(){
    Animated.timing(
      this.state.opacity,
      {
        toValue: 1,
        duration: 500
      }
    ).start();
  }

  render() {
    return (
      <Animated.View style={{ flex: 1, opacity: this.state.opacity }}>
        <ProfileLayout user={ this.props.user } back={() => this.props.navigation.goBack()}>
          <Card>
            <ToolBar title={ this.props.user.name } icon="account-circle"/>
            <FlatList
              data={ this.state.infoList }
              renderItem={ ({item}) => <SimpleItem {...item} onPress={ this._exit }/> }
              ItemSeparatorComponent={ () => <Separator color="#eaeaea"/> }
              />
          </Card>
        </ProfileLayout>
      </Animated.View>
    );
  }
}

export default connect(setProps)(Profile);
