import React from 'react';

import LoginLayout from './login';
import ProfileLayout from './profile';
import HomeLayout from './home';
import IlluminationLayout from './illumination';

export { ProfileLayout, LoginLayout, HomeLayout, IlluminationLayout };
