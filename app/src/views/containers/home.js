import React from 'react';
import { StyleSheet, ScrollView, View , TouchableWithoutFeedback } from 'react-native';
import Header from "../components/header";
import { ContentRight } from "../components/globals";

HomeLayout = (props) => {
  return (
    <View>
      <Header>
        <TouchableWithoutFeedback onPress={ props.action }>
          <View>
            <ContentRight text={ props.user.first_name + props.user.last_name } actionIcon="account-circle"/>
          </View>
        </TouchableWithoutFeedback>
      </Header>
      <ScrollView>
        <View style={ styles.content }>
          { props.children }
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  scroll:{
    width: "100%"
  },
  content: {
    flexDirection: "row",
    flexWrap: "wrap"
  }
})

export default HomeLayout;
