import React from 'react';
import { StyleSheet, ScrollView, View, TouchableWithoutFeedback } from 'react-native';
import Header from "../components/header";
import { ContentRight } from "../components/globals";

IlluminationLayout = (props) => {
  return (
    <View>
      <Header title="Iluminación" showBack={ true } onBack={ props.back }>
        <TouchableWithoutFeedback onPress={ props.action }>
          <View>
            <ContentRight text={ props.user.first_name + props.user.last_name } actionIcon="account-circle"/>
          </View>
        </TouchableWithoutFeedback>
      </Header>

      <View style={ styles.content }>
        { props.children }
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    padding: 20
  }
})

export default IlluminationLayout;
