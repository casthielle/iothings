import React from 'react';
import { StyleSheet, ScrollView, View } from 'react-native';
import Header from "../components/header";
import { ContentRight } from "../components/globals";

ProfileLayout = (props) => {
  return (
    <View>
      <Header showBack={ true } title="Mi Perfil" onBack={ props.back }/>
      <View style={ styles.content }>
        { props.children }
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    padding: 20
  }
})

export default ProfileLayout;
