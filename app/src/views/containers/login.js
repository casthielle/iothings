import React from 'react';
import { Button, StyleSheet, Text, Image, View } from 'react-native';

LoginLayout = (props) => {
  return (
    <View style={ styles.container }>
      <Image source={ require('../../assets/logo-centrado.png') } style={ styles.logo } />
      <Text style={ styles.title }>THE CLOUD COMPANY</Text>
      <Text style={ styles.subtitle }>INTERNAL APPS</Text>
      { props.children }
      <View>
        <Button title="Entrar" onPress={ props.onSubmit } style={ styles.button }/>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  button:{
    padding: 30
  },
  logo: {
    width: 180,
    height: 200,
    resizeMode: 'contain',
  },
  title: {
    color: "#2a66ab",
  },
  subtitle: {
    fontSize: 12,
    marginVertical: 20,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});

export default LoginLayout;
