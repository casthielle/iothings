import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Alert } from 'react-native';
import { InputEmail, InputPassword } from './components/globals';
import { LoginLayout } from './containers/layouts';
import Firebase from '../apis/firebase';

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      firebase: Firebase.app(),
      firestore: Firebase.firestore(),
      auth: Firebase.auth(),
      login: {
        user: "",
        password: ""
      }
    };
  }

  login = async () => {
    let user = this.state.login.user;
    let password = this.state.login.password;
    let process = await this.state.auth.signInWithEmailAndPassword(user, password).catch((error) => {
      Alert.alert(
        'Error',
        'Credenciales Invalidas. Por favor verifique e intente de nuevo.',
        [ {text: 'Acceptar', onPress: () => {}}, ],
        { cancelable: true }
      );
    });
    let docRef = this.state.firestore.collection("users").doc(process.user.uid);
    docRef.get().then((doc) => {
      if(doc.exists){
        this.props.dispatch({
          type: "SET_USER_DATA",
          payload: {
            user: {...doc.data(), status: 'online'}
          }
        });
        this.props.navigation.navigate('home');
      }
      else{
        console.log("No such document!");
      }
    }).catch((error) => {
        console.log("Error getting document:", error);
    });
  }
  handlerChangeEmail = (user) => {
    return this.setState((state) => { state.login.user = user; return state.login; });
  }
  handlerChangePass = (password) => {
    return this.setState((state) => { state.login.password = password; return state.login; });
  }

  render() {
    return (
      <LoginLayout onSubmit={ this.login }>
        <InputEmail onChange={ this.handlerChangeEmail } value={ this.state.login.user } />
        <InputPassword onChange={ this.handlerChangePass } value={ this.state.login.password } />
      </LoginLayout>
    );
  }
}

export default connect(null)(Login);
