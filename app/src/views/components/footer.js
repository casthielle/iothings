import React from 'react';
import { StyleSheet, Text, Image, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Footer = (props) => {
    return (
      <View style={ styles.footer }>
          <View style={ styles.container }>
            { props.children }
          </View>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    paddingVertical: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  footer: {
    backgroundColor: '#f5f5f5'
  }
});

export default Footer;
