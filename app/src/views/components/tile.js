import React from 'react';
import { StyleSheet, View, TouchableNativeFeedback, Text } from 'react-native';

const Tile = (props) => {
    return (
      <TouchableNativeFeedback onPress={ props.onPress }>
        <View  style={ styles.tile }>
          <Text>
            { props.text }
          </Text>
        </View>
      </TouchableNativeFeedback>
    );
}

const styles = StyleSheet.create({
  tile: {
    width: 175,
    height: 175,
    backgroundColor: '#f5f5f5',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: 20,
    marginLeft: 20,
    marginLeft: 20,
    padding: 40
  }
});

export default Tile;
