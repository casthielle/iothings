import React from 'react';
import { StyleSheet, View, TouchableNativeFeedback, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


const ToolBar = (props) => {
  return (
    <TouchableNativeFeedback onPress={ props.onPress }>
      <View style={ styles.toolbar }>
        <View style={ styles.left }>
          <Icon color="#000" name={ props.icon } style={ styles.icon }/>
          <Text style={ styles.title }> { props.title }</Text>
          <Text> { props.subtitle }</Text>
        </View>
        { props.children }
      </View>
    </TouchableNativeFeedback>
  )
}

const styles = StyleSheet.create({
  icon: {
    fontSize: 15,
    marginRight: 10
  },
  left: {
    flexDirection: "row",
    alignItems: "center",
    padding: 20
  },
  toolbar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    fontWeight: 'bold',
    color: "#000"
  }
});


export default ToolBar;
