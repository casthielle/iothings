import React from 'react';
import { StyleSheet, View, TouchableNativeFeedback, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Item = (props) => {
  return (
    <TouchableNativeFeedback onPress={ props.onPress }>
      <View style={ styles.content }>
        <View style={ styles.left }>
          <Icon name="fiber-manual-record" color={ (props.state) ? "#2ec1f0" : "#c7c7c7" } style={ styles.icon }/>
          <Text> { props.text } </Text>
        </View>
        { props.children }
      </View>
    </TouchableNativeFeedback>
  );
}

const SimpleItem = (props) => {
  return (
    <TouchableNativeFeedback onPress={ () => { if(props.type == "item-touchable") props.onPress(); } }>
      <View style={ styles.content }>
        <View style={ (props.type == "item-list") ? styles.justify : styles.center }>
          <Text style={ (props.type == "item-list") ? styles.bold : styles.hide }> { props.property } </Text>
          <Icon name="exit-to-app" color="#000" style={ (props.type == "item-list") ? styles.hide : {} } />
          <Text style={ (props.type == "item-list") ? {} : styles.bold }> { props.value } </Text>
        </View>
      </View>
    </TouchableNativeFeedback>
  );
}

const EmptyItem = (props) => {
  return (
    <View style={ styles.content }>
      <View style={ styles.left }>
        <Text> { props.text } </Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  justify: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 20
  },
  center: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    padding: 20
  },
  bold: {
    color: "#000"
  },
  hide: {
    display: "none"
  },
  icon: {
    fontSize: 15,
    marginRight: 10
  },
  content:{
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  left: {
    flexDirection: "row",
    alignItems: "center",
    padding: 20
  },
});


export { Item, EmptyItem, SimpleItem };
