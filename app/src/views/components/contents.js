import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Button } from "./globals";

const ContentRight = (props) => {
  return (
    <View style={ styles.right }>
      <Text> { props.text } </Text>
      <Button name={ props.actionIcon } color="#000" style={ styles.userButton } onPress={ props.onAction }/>
    </View>
  );
}

const ContentLeft = (props) => {
  return (
    <View style={ styles.left }>
      <Button name={ props.actionIcon } color="#000" style={ styles.userButton } onPress={ props.onAction }/>
      <Text> { props.text } </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  userButton:{
    marginLeft: 15,
    marginRight: 15
  },
  right: {
    paddingVertical: 20,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  left: {
    paddingVertical: 20,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  }
})

export { ContentRight, ContentLeft };
