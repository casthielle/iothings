import React from 'react';
import { StyleSheet, Text, Image, View, SafeAreaView, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Button } from "./globals";

const Header = (props) => {
    return (
      <View style={ styles.header }>
        <SafeAreaView>
          <View style={ styles.container }>
            <TouchableWithoutFeedback onPress={ props.onBack }>
              <View style={ styles.left }>
                <Button name="keyboard-arrow-left" color="#000" style={ (props.showBack == true) ? styles.backButton : styles.hide } onPress={ props.onBack }/>
                <Image source={ require('../../assets/logo-apaisado.png') } style={ (props.title) ? styles.hide : styles.logo } />
                <Text style={ (props.title) ? styles.title : styles.hide }> { props.title } </Text>
              </View>
            </TouchableWithoutFeedback>
            { props.children }
          </View>
        </SafeAreaView>
      </View>
    );
}

const styles = StyleSheet.create({
  title: {
    color: "#666"
  },
  backButton: {
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: '#f5f5f5',
  },
  hide: {
    display: 'none'
  },
  logo: {
    width: 80,
    height: 30,
    resizeMode: 'contain',
    marginLeft: 15
  },
  left: {
    paddingVertical: 20,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: '100%',
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  header: {
    backgroundColor: '#f5f5f5',
  }
});

export default Header;
