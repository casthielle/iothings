import React from 'react';
import { StyleSheet, View, Modal } from 'react-native';
import Header from "./header";
import Footer from "./footer";
import { ContentRight } from "./contents";


const Dialog = (props) => {
  return (
    <Modal animationType="slide" transparent={ false } visible={ props.visible } onRequestClose={ () => { } }>
      <Header showBack={ true } title={ props.title } onBack={ props.onClose }>
        <ContentRight onAction={ props.onAction } actionIcon={ props.icon || "close" }/>
      </Header>
      <View style={ styles.content }>
        { props.children }
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  content: {
    padding: 20
  }
});

export { Dialog };
