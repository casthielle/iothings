import React from 'react';
import { StyleSheet, View } from 'react-native';


const Card = (props) => {
  return (
    <View style={ (props.scroll) ? styles.cardScroll : styles.card }>
      { props.children }
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: "#fff",
    marginBottom: 20,
  },
  cardScroll:{
    backgroundColor: "#fff",
    marginBottom: 20,
    maxHeight: "75%"
  }
});

export { Card };
