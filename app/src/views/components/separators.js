import React from 'react';
import { StyleSheet, View } from 'react-native';

const Separator = (props) => {
  return (
    <View style={[ styles.separator, { borderTopColor: props.color || '#eaeaea'} ]}>
    </View>
  )
}

const styles = StyleSheet.create({
  separator: {
    borderTopWidth: 1
  }
});


export default Separator;
