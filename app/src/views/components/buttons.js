import React from 'react';
import { StyleSheet, Text, View, TouchableNativeFeedback} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Button = (props) => {
  return (
    <TouchableNativeFeedback onPress={ props.onPress }>
      <View style={ props.style ? props.style : styles.default }>
        <Icon name={ props.name } color={ props.color }/>
      </View>
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  default: {
    backgroundColor: '#f5f5f5',
    height: "100%",
    width: 50,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  }
});

export default Button;
