import React from 'react';
import { StyleSheet, TextInput } from 'react-native';

const InputEmail = (props) => {
  return (
    <TextInput
      autoCapitalize="none"
      returnKeyType="next"
      textContentType="emailAddress"
      keyboardType="email-address"
      contextMenuHidden
      placeholder="Email"
      onChangeText={ props.onChange }
      value={ props.value }
      style={ styles.inputBase } />
  )
}

const InputText = (props) => {
  return (
    <TextInput
      autoCapitalize="none"
      placeholder={ props.placeholder }
      onChangeText={ props.onChange }
      value={ props.value }
      style={ {...styles.inputBlock, backgroundColor: props.backgroundColor} } />
  )
}

const InputPassword = (props) => {
  return (
    <TextInput
      autoCapitalize="none"
      textContentType="password"
      secureTextEntry
      contextMenuHidden
      placeholder="Password"
      onChangeText={ props.onChange }
      value={ props.value }
      style={ styles.inputBase } />
  )
}

const styles = StyleSheet.create({
  inputBase: {
    width: "90%",
    textAlign: "center",
    marginBottom: 10
  },
  inputBlock: {
    textAlign: "center",
    marginBottom: 10
  }
});

export { InputEmail, InputPassword, InputText };
