import React, { Component } from 'react';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';
import { connect } from 'react-redux';

import Login from './views/login';
import Profile from './views/profile';
import Home from './views/home';
import Illumination from './views/illumination';
import Loading from './views/components/loading';

setProps = (state) => { return { user: state.user }; };

class LoadingScreen extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount(){
    let user = this.props.user;
    if(user){
      this.props.navigation.navigate((user.status == 'online') ? 'App' : 'Auth');
    }
  }
  render() {
    return (
      <Loading/>
    );
  }
}

const AppStack = createStackNavigator({
    home: { screen: Home },
    illumination: { screen: Illumination },
    profile: { screen: Profile }
  }, {
    initialRouteName: 'home',
    headerMode: 'none'
});

const AuthStack = createStackNavigator({ login: Login }, { headerMode: 'none' });

const RootStack = createSwitchNavigator({
    Loading: connect(setProps)(LoadingScreen),
    App: AppStack,
    Auth: AuthStack,
  }, {
    initialRouteName: 'Loading',
    headerMode: 'none'
});

export default RootStack;
