import config from "../config";

class Api {

  token = undefined;

  _base_api = "https://api.particle.io";

  _settings = config.particle;

  _endpoints = {
    oauth: this._base_api + "/oauth/token",
    process: this._base_api + "/v1/devices/"+this._settings.deviceId+"/:function"
  }

  getToken = async () => {
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let request = new Request(this._endpoints.oauth, {
      method: "POST",
      headers: headers,
      body: "expires_in=3600&grant_type=password&username="+this._settings.username+"&password="+this._settings.password+"&client_id="+this._settings.clientId+"&client_secret="+this._settings.clientSecret
    });

    let query = await fetch(request);
    let data = await query.json();

    return data;
  }

  process = async (settings) => {
    this.token = this.token || await this.getToken();
    let headers = new Headers({
      "Authorization": 'Bearer '+ this.token.access_token,
      "Content-Type": "application/json"
    });
    let request = new Request(this._endpoints.process.replace(":function", settings.fn), {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(settings.body),
    });
    let query = await fetch(request);
    let data = await query.json();
    return data;
  }
}

export default new Api();
