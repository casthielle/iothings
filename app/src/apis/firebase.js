import config from "../config";

class Fire {

  _firebase = null;

  app = () => {

    this._firebase = require("firebase");
    require("firebase/firestore");

    if(this._firebase.apps.length == 0){
      this._firebase.initializeApp(config.firebase);
    }
    return this._firebase;
  }

  auth = () => this._firebase.auth();

  firestore = () => {
    let firestore = this._firebase.firestore();
    firestore.settings({ timestampsInSnapshots: true });
    return firestore;
  }
}


export default new Fire();
