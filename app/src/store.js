import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

// const store = createStore(login);

const settings = {
  key: "root",
  storage
};

const reducer = (state = {}, action) => {
  switch (action.type) {
    case "SET_USER_DATA":
      return {...state, user: action.payload.user};
    default:
      return state;
  }
}
const peReducer = persistReducer(settings, reducer);
const store = createStore(peReducer, { user: {} });
const persistor = persistStore(store);

export { store, persistor };
